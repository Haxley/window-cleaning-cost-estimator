## Price Calculation Example

## Ideally, square footage and levels should be pulled from a
#  property infromation API, such as Zillow. Additions most
#  likely cannot be pulled.
sf = input("Enter the square footage:(number only) ")
levels = input("Is your home more than one level?(y/n): ")

## Additions
cutUp = input("Do have some cut-up windows? (y/n): ")
ss = input("Do you have some sunscreens?(y/n): ")

## Squarefootage truncation for base price
basePrice = int((sf[:3]))

## Base Price Constant
basePriceOriginal = basePrice 

## First base price modification based on sf range
# 4000sf or higher
if int(sf) >= 4000:
    bigPrice = basePrice * .7
    basePrice *= .7
    
# 3000sf or higher
elif int(sf) >= 3000:
    medPrice = basePrice * .7
    basePrice *= .7

# 2000sf or higher
elif int(sf) >= 2000:
    smallPrice = basePrice *.7
    basePrice *= .7

# 1000sf or higher    
elif int(sf) >= 1000:
    tinyPrice = basePrice *.7
    basePrice *= .7
    
# Less than 1000sf
else:
    minPrice = basePrice * .7
    basePrice *= .7

## Second base price modification, from levels.
if levels in ['yes', 'yeah', 'y', 'Yes', 'YES', 'Y']:
    
    ## Multi-level multiplier
    mlPrice = basePrice * 1.1   
    basePrice *= 1.1 
else: 
    
    ## Single-level multiplier
    slPrice =  basePrice * 1
    basePrice *= 1  

## Third base price modification, from cut-up windows.
if cutUp in ['yes', 'yeah', 'y', 'Yes', 'YES', 'Y']:
    
    cuPercent = input("What percent of the windows are cut-up windows?:")
    
    ## Cut Up 80%+ multiplier
    if int(cuPercent) > 80:
        cuPrice = basePrice * 1.3 
        basePrice *= 1.3
        
    ## Cut Up 40%+ multiplier    
    elif int(cuPercent) > 40:
        cuPrice = basePrice * 1.2
        basePrice *= 1.2
        
    ## Cut Up 10%+ multiplier    
    elif int(cuPercent) > 10:
        cuPrice = basePrice * 1.1 
        basePrice *= 1.1
        
    ## <=10%, no multiplier, they probably are bug screens    
    else:
        cuPrice = basePrice;

## Fourth base price modification, from sunscreen
if ss in ['yes', 'yeah', 'y', 'Yes', 'YES', 'Y']:
    ssPercent = input("What percent of the windows have sunscreens?(number only):")
    
    ## Sunscreens 80%+ multiplier
    if int(ssPercent) > 80:
        ssPrice = basePrice * 1.3 
        ssOnlyPrice = int(basePrice * 1.3 - basePrice)
        basePrice *= 1.3
        
    ## Sunscreens 40%+ multiplier    
    elif int(ssPercent) > 40:
        ssPrice = basePrice * 1.2
        ssOnlyPrice = int(basePrice * 1.2 - basePrice)
        basePrice *= 1.2
        
    ## Sunscreens 10%+ multiplier    
    elif int(ssPercent) > 10:
        ssPrice = basePrice * 1.1 
        ssOnlyPrice = int(basePrice * 1.1 - basePrice)
        basePrice *= 1.1
        
    ## <=10%, no multiplier, they probably are bug screens    
    else:
        ssOnlyPrice = 0
        ssPrice = basePrice;
        
## Exterior only multiplier
outOnly = basePrice * .6

## Truncate prices
inOutPrice = int(basePrice)
outOnlyPrice = int(outOnly)

## Round prices to nearest $5 function
def myRound(x, base=5):
    return int(base * round(float(x)/base))

## Use rounding function to get final price    
finalInOutPrice = myRound(inOutPrice)
finalOutOnlyPrice = myRound(outOnlyPrice)

## Print calculations
print("---------------------------------------------")

if levels in ['yes', 'yeah', 'y', 'Yes', 'YES', 'Y']:
    print ("Best guess cost for this multi-level home: $" + str(mlPrice))
else:
    print ("Best guess cost for this single-level home: $" + str(slPrice))

if cutUp in ['yes', 'yeah', 'y', 'Yes', 'YES', 'Y']:
    print ("Then with cut-up windows: $" + str(cuPrice))

if ss in ['yes', 'yeah', 'y', 'Yes', 'YES', 'Y']:
    print ("Additional cost of sunscreens: $" + str(ssOnlyPrice))
    print ("Subtotal with sunscreens: $" + str(ssPrice))
    
finalPrice = basePrice
print("---------------------------------------------")
print("Final Estimate In & Out: $" + str(finalInOutPrice * .75) + " - $" + str(finalInOutPrice * 1.25) )
print("Final Estimate Out Only: $" + str(finalOutOnlyPrice * .75) + " - $" + str(finalOutOnlyPrice * 1.25) )

print("---------------------------------------------")